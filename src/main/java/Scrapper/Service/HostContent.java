package Scrapper.Service;

import Scrapper.App;
import Scrapper.Model.WebSite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

@Service
public class HostContent implements HttpClient{

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public String get(WebSite webSite) {
        RestTemplate rest = new RestTemplate();

        log.info(String.format("URI to send GET: %s", webSite.host));

        return rest.getForObject(webSite.host, String.class);
    }

    public HttpHeaders head(String host) throws RestClientException, URISyntaxException{
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        log.info(String.format("URI to send HEAD: %s", host));

        headers = rest.headForHeaders(new URI(host));

        return headers;
    }
}
