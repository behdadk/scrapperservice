package Scrapper.Service;

import Scrapper.Model.WebSite;
import org.springframework.http.HttpHeaders;

import java.net.URISyntaxException;

public interface HttpClient {
    public String get(WebSite webSite);

    public HttpHeaders head(String host) throws URISyntaxException;
}
