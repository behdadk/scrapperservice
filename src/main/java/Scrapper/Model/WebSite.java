package Scrapper.Model;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.jdbc.core.RowMapper;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class WebSite extends ResourceSupport {
    public int id;

    public String host;

    public boolean scrapped = false;

    public Date scrappedAt;

    public int rank;

    public String content;

    public WebSite(int id, String host, boolean scrapped, Date scrappedAt, int rank, String content) {
        this.id = id;
        this.host = host;
        this.scrapped = scrapped;
        this.scrappedAt = scrappedAt;
        this.rank = rank;
        this.content = content;
    }

    public WebSite(){};
}
