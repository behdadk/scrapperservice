package Scrapper.Controller;

import Scrapper.App;
import Scrapper.Model.WebSite;
import Scrapper.Repository.DataStorage;
import Scrapper.Service.HttpClient;
import Scrapper.UseCase.Process;
import Scrapper.UseCase.Ranking;
import Scrapper.UseCase.Request.ProcessRequest;
import Scrapper.UseCase.Request.StoreRanking;
import Scrapper.UseCase.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@EnableAsync
@RestController
public class Scrap {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    @Autowired
    DataStorage storage;

    @Autowired
    HttpClient httpClient;

    @Autowired
    Process processor;

    @RequestMapping(path="/scrap", method=RequestMethod.GET)
    public HttpEntity<List<WebSite>> get() {
        List<WebSite> scrappedWebSites = this.storage.getAll();

        return new ResponseEntity<List<WebSite>>(scrappedWebSites, HttpStatus.OK);
    }

    @RequestMapping(
            path="/scrap",
            method=RequestMethod.POST
    )

    public ResponseEntity<Void> post(@RequestBody WebSite webSite) {

        log.info(webSite.host);

        ProcessRequest request = new ProcessRequest();
        request.webSite = webSite;

        processor.execute(request);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
}
