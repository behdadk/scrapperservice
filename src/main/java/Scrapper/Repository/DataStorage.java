package Scrapper.Repository;

import Scrapper.Model.WebSite;

import java.util.List;

public interface DataStorage {

    public int save(WebSite webSite);

    public List<WebSite> getAll();
}
