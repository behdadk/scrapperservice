package Scrapper.Repository;

import Scrapper.Model.WebSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

@Repository
public class Memory implements DataStorage {

    JdbcTemplate jdbc;

    @Autowired
    public Memory(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
        this.initDatabase();
    }

    private void initDatabase() {
        this.jdbc.execute(
                "CREATE TABLE Scrap(" +
                "ID INT AUTO_INCREMENT PRIMARY KEY, " +
                        "host VARCHAR(255) DEFAULT NULL , " +
                        "scrappedAt TIMESTAMP DEFAULT NULL , " +
                        "rank INT DEFAULT NULL, " +
                        "scrapped BIT DEFAULT 0, " +
                        "content LONGTEXT DEFAULT NULL )"
        );
    }

    @Override
    public int save(WebSite webSite) {
        return this.jdbc.update(
                "INSERT INTO Scrap (host, scrappedAt, scrapped, rank, content) VALUES(?, ?, ?, ?, ?)",
                new Object[] {webSite.host, webSite.scrappedAt, webSite.scrapped, webSite.rank, webSite.content}
        );
    }

    @Override
    public List<WebSite> getAll() {
       List<WebSite> webSites = this.jdbc.query(
               "SELECT * FROM Scrap",
               (rs, rowNum) -> new WebSite(
                       rs.getInt("ID"),
                       rs.getString("host"),
                       rs.getBoolean("scrapped"),
                       rs.getDate("scrappedAt"),
                       rs.getInt("rank"),
                       rs.getString("content")
               )
       );

       return webSites;
    }
}
