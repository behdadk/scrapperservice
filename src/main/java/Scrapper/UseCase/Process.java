package Scrapper.UseCase;


import Scrapper.Model.WebSite;
import Scrapper.Repository.DataStorage;
import Scrapper.Service.HttpClient;
import Scrapper.UseCase.Request.ProcessRequest;
import Scrapper.UseCase.Request.StoreRanking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@EnableAsync
@Service
public class Process {

    HttpClient httpClient;

    DataStorage storage;

    @Autowired
    public Process(HttpClient httpClient, DataStorage storage) {
        this.httpClient = httpClient;
        this.storage = storage;
    }

    @Async
    public void execute(ProcessRequest request) {
        WebSite webSite = request.webSite;
        Scrapper.UseCase.Request.Ranking rankingRequest = new Scrapper.UseCase.Request.Ranking();
        rankingRequest.webSite = webSite;
        Ranking ranking = new Ranking(httpClient);
        webSite = ranking.execute(rankingRequest);

        StoreRanking storeRequest = new StoreRanking();
        storeRequest.webSite = webSite;
        Store store = new Store(this.storage);
        store.execute(storeRequest);
    }

}
