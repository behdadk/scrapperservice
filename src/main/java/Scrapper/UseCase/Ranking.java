package Scrapper.UseCase;

import Scrapper.App;
import Scrapper.Model.WebSite;
import Scrapper.Service.HttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;

public class Ranking {

    private HttpClient httpClient;

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public Ranking(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public WebSite execute(Scrapper.UseCase.Request.Ranking request) {
        WebSite webSite = request.webSite;

        List<String> hostLinks = this.getLinks(webSite.host);

        webSite.content = "Content";
        webSite.rank = this.processRank(hostLinks);
        webSite.scrappedAt = Calendar.getInstance().getTime();
        webSite.scrapped = true;

        return webSite;
    }

    private int processRank(List<String> links) {
        int rank = 0;

        for (int i=0; i!=links.size(); ++i) {
            try {
                this.httpClient.head(links.get(i));
            } catch (Exception e) {
                log.error(e.getMessage());
                rank++;
                log.info(String.format("Current rank: %d", rank));
            }
        }

        return rank;
    }

    private List<String> getLinks(String host) {
        List<String> hostLinks = new ArrayList<String>();
        try {
            Document doc = Jsoup.connect(host).get();
            Elements links = doc.select("a[href]");

            for (Element link : links) {
                hostLinks.add(link.attr("abs:href"));
            }

        } catch (IOException exception) {
            log.error(exception.getMessage());
        }

        return hostLinks;
    }
}
