package Scrapper.UseCase;

import Scrapper.Repository.DataStorage;
import Scrapper.UseCase.Request.StoreRanking;
import org.springframework.beans.factory.annotation.Autowired;

public class Store {

    private DataStorage storage;

    public Store(DataStorage storage) {
        this.storage = storage;
    }

    public void execute(StoreRanking request) {
        this.storage.save(request.webSite);
    }
}
